
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include "quotient.hpp"

using quotient::Int;


template<typename T>
void vector_to_file(std::string filename, const std::vector<T>& v){
  std::ofstream out(filename,std::ofstream::binary);
  out.write((char*)v.data(),v.size()*sizeof(T));
}

void laplace2d_graph(Int nx,Int ny,quotient::CoordinateGraph& graph){
  //graph.Resize(nx*ny);
  auto id = [&](Int ix,Int iy){
    return iy + ny*ix;
  };
  for(Int ix=0;ix<nx;ix++){
    for(Int iy=0;iy<ny;iy++){
      graph.AddEdge(id(ix,iy),id(ix,iy));
      if(ix>0)
        graph.AddEdge(id(ix,iy),id(ix-1,iy));
      if(ix<nx-1)
        graph.AddEdge(id(ix,iy),id(ix+1,iy));
      if(iy>0)
        graph.AddEdge(id(ix,iy),id(ix,iy-1));
      if(iy<ny-1)
        graph.AddEdge(id(ix,iy),id(ix,iy+1)); 
    }
  }
}

int main(int argc,char** argv){
  Int n=32;
  Int nx=n;
  Int ny=n;
  quotient::CoordinateGraph graph;
  graph.Resize(nx*ny);
  laplace2d_graph(nx,ny,graph);
  quotient::MinimumDegreeControl control;
  control.degree_type = quotient::kExactDegree;
  control.force_minimal_pivot_indices = true;
  quotient::QuotientGraph quotient_graph(graph, control);
  quotient::MinimumDegreeResult analysis =
      quotient::MinimumDegree(&quotient_graph);


  quotient::Buffer<Int> inverse_permutation;
  quotient_graph.ComputePostorder(&inverse_permutation);

  std::vector<Int> permutation(inverse_permutation.Size());
  std::copy(inverse_permutation.begin(),inverse_permutation.end(),permutation.begin());
  vector_to_file("permutation.dat",permutation);






  return 0;
}
