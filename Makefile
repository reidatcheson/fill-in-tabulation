#my_sources:=$(shell find -iname '*.cpp')
#OBJS=$(my_sources:%.cpp=%.o)

CXX := g++ 
#CXXFLAGS := -fopenmp -O3 --std=c++14 -I /home/reida/clones/quotient/include -I/home/reida/clones/mantis/include -DQUOTIENT_USE_64BIT
CXXFLAGS := -g --std=c++14 -I /home/reida/clones/quotient/include -I/home/reida/clones/mantis/include -DQUOTIENT_USE_64BIT


all : main main_reordered

main : main.cpp
	$(CXX) $(CXXFLAGS) main.cpp -o main

main_reordered : main_reordered.cpp
	$(CXX) $(CXXFLAGS) main_reordered.cpp -o main_reordered


.PHONY : clean


clean :
	rm -rf ./main
	rm -rf ./main_reordered
	rm -rf ./*.svg
	rm -rf ./*.pdf
	rm -rf ./*.dat
