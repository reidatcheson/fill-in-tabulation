import numpy as np
import scipy.sparse as sp
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt

def lap1d(n):
    A=sp.lil_matrix((n,n))
    A[0,0]=-2.0
    A[0,1]=1.0
    A[n-1,n-1]=-2.0
    A[n-1,n-2]=1.0
    for i in range(1,n-1):
        A[i,i-1]=1.0
        A[i,i]=-2.0
        A[i,i+1]=1.0
    return A

def lap2d(nx,ny):
    Ix=sp.eye(nx,nx)
    Iy=sp.eye(ny,ny)
    Ax=lap1d(nx)
    Ay=lap1d(ny)
    return sp.kron(Ix,Ay) + sp.kron(Ax,Iy)

nx=32
ny=32
A=lap2d(nx,ny)
plt.spy(A,markersize=0.4)
plt.savefig("before.svg")

plt.cla()

p=np.fromfile("permutation.dat",dtype=np.int64)
print(p)
plt.spy(A[np.ix_(p,p)],markersize=0.4)
plt.savefig("after.svg")
