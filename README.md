# Tabulating fill-in estimates


I find it useful to be able to compute fill-in estimates for different
sparsity pattern matrices arising from stencil calculations.

I use the [Quotient](https://gitlab.com/hodge_star/quotient) library by Jack Poulson 
to compute these.



