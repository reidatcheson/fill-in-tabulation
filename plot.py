import numpy as np
import matplotlib
matplotlib.use("agg")
import matplotlib.pyplot as plt


ndofs=np.fromfile("ndofs.dat",dtype=np.int64)
nnzs=np.fromfile("nnz.dat",dtype=np.int64)

plt.plot(ndofs,nnzs)
plt.title("2D Laplace")
plt.xlabel("Total degrees of freedom: $n=nx*ny$")
plt.ylabel("Nonzeros in Cholesky Factor")
plt.savefig("laplace2d_fillin.svg")
